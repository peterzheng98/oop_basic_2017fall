/*
 * File: Basic.cpp
 * ---------------
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * This file is the starter project for the BASIC interpreter from
 * Assignment #6.
 * [TODO: extend and correct the documentation]
 */

#include <cctype>
#include <iostream>
#include <string>
#include "exp.h"
#include "parser.h"
#include "program.h"
#include "../StanfordCPPLib/error.h"
#include "../StanfordCPPLib/tokenscanner.h"

#include "../StanfordCPPLib/simpio.h"
#include "../StanfordCPPLib/strlib.h"
using namespace std;

/* Function prototypes */

void processLine(string line, Program & program, EvalState & state);

/* Main program */

int main() {
   EvalState state;
   Program program;
   //cout << "Stub implementation of BASIC" << endl;
   while (true) {
      try {
		  /*string st;
		  getline(cin, st);
		  TokenScanner scanner;
		  scanner.ignoreWhitespace();
		  scanner.scanNumbers();
		  scanner.setInput(st);
		  while (scanner.hasMoreTokens()) {
			  string token = scanner.nextToken();
			  cout << token << endl;
		  }
         //*/
		  processLine(getLine(), program, state);
      } catch (ErrorException & ex) {
         cout << ex.getMessage() << endl;
      }
   }
   return 0;
}

/*
 * Function: processLine
 * Usage: processLine(line, program, state);
 * -----------------------------------------
 * Processes a single line entered by the user.  In this version,
 * the implementation does exactly what the interpreter program
 * does in Chapter 19: read a line, parse it as an expression,
 * and then print the result.  In your implementation, you will
 * need to replace this method with one that can respond correctly
 * when the user enters a program line (which begins with a number)
 * or one of the BASIC commands, such as LIST or RUN.
 */
void printHelpMessage() {
	cout << "Please Refer To Basic Grammars.\n";
}


void processLine(string line, Program & program, EvalState & state) {
	TokenScanner scanner;
	scanner.ignoreWhitespace();
	scanner.scanNumbers();
	scanner.setInput(line);
	string token = scanner.nextToken();
	TokenType t = scanner.getTokenType(token);
	if (token == "QUIT") {
		exit(0);
	}
	else if (token == "LIST") {
		int LineNumber = program.getFirstLineNumber();
		while (LineNumber != __LINE_NO_MORE && program.getSourceLine(LineNumber) != __LINE_NOT_FOUND_ERROR) {
			cout << program.getSourceLine(LineNumber) << endl;
			LineNumber = program.getNextLineNumber(LineNumber);
		}
	}
	else if (token == "RUN") {
		int LineNumber = program.getFirstLineNumber();
		while (LineNumber != __LINE_NO_MORE && LineNumber != __STOP_SIG) {
			//cout << "CurLineNumber" << LineNumber << endl;
			if (!program.queryLineNumber(LineNumber)) error("LINE NUMBER ERROR");
			Statement* stmt = program.getParsedStatement(LineNumber);
			stmt->execute(state);
			int p = stmt->returnLineNumber(0);
			if (p == __NO_RETURN) LineNumber = program.getNextLineNumber(LineNumber);
			else LineNumber = p;
		}
	}
	else if (token == "CLEAR") {
		program.clear();
		state.clear();
	}
	else if (token == "HELP") {
		printHelpMessage();
	}
	else if (token == "PRINT") {
		PrintStatement* stmt = new PrintStatement(line);
		stmt->execute(state);
	}
	else if (token == "LET") {
		if (line != "LET LET") {
			LetStatement* stmt = new LetStatement(line);
			stmt->execute(state);
		}
	}
	else if (token == "INPUT") {
		InputStatement* stmt = new InputStatement(line);
		stmt->execute(state);
	}
	else if (t == NUMBER && scanner.hasMoreTokens()) {
		int LineNumber = stringToInteger(token);
		program.addSourceLine(LineNumber, line);
		stringstream ss(line);
		string tmp;
		ss >> tmp;
		string remainLine;
		getline(ss, remainLine);
		//while (scanner.hasMoreTokens()) {
			string t2 = scanner.nextToken();
			if (t2 == "LET") {
				LetStatement* stmt = new LetStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "INPUT") {
				InputStatement* stmt = new InputStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "REM") {
				RemStatement* stmt = new RemStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "PRINT") {
				PrintStatement* stmt = new PrintStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "END") {
				EndStatement* stmt = new EndStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "IF") {
				IfStatement* stmt = new IfStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
			else if (t2 == "GOTO") {
				GotoStatement* stmt = new GotoStatement(remainLine);
				program.setParsedStatement(LineNumber, stmt);
			}
		//}
	}
	else if (t == NUMBER && !scanner.hasMoreTokens()) {
		program.removeSourceLine(stringToInteger(token));
	}
	else if (token != "") cout << "here!" << endl,  error("SYNTAX ERROR");
}
