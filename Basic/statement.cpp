/*
 * File: statement.cpp
 * -------------------
 * This file implements the constructor and destructor for
 * the Statement class itself.  Your implementation must do
 * the same for the subclasses you define for each of the
 * BASIC statements.
 */

#include <string>
#include "statement.h"
#include "../StanfordCPPLib/tokenscanner.h"
#include "parser.h"
#include "program.h"
using namespace std;

/* Implementation of the Statement class */

Statement::Statement() {
   /* Empty */
}

Statement::~Statement() {
   /* Empty */
}

LetStatement::LetStatement(string line)
{
	this->line = line;
}

void LetStatement::execute(EvalState & state)
{
	TokenScanner scanner;
	scanner.setInput(this->line);
	scanner.ignoreWhitespace();
	string token = scanner.nextToken();
	token = scanner.nextToken();
	if (token == "LET") error("SYNTAX ERROR"); else
	{
		stringstream ss(this->line);
		string word;
		string remainLine;
		ss >> word;
		getline(ss, remainLine);
		scanner.setInput(remainLine);
		Expression *exp = parseExp(scanner);
		int value = exp->eval(state);
		delete exp;
	}
}

int LetStatement::returnLineNumber(int num)
{
	return __NO_RETURN;
}

IfStatement::IfStatement(string line)
{
	this->line = line;
	this->nextLineNumber = __LINE_NO_MORE;
}

void IfStatement::execute(EvalState & state)
{
	int number, type;
	if (line.find("=") != -1)       number = line.find("="), type = 0;
	else if (line.find("<") != -1)  number = line.find("<"), type = 1;
	else if (line.find(">") != -1)  number = line.find(">"), type = 2;
	string leftExpr = line.substr(3, number - 3);
	TokenScanner scanner;
	scanner.setInput(leftExpr);
	scanner.ignoreWhitespace();
	Expression *lexp = parseExp(scanner);
	int lvalue = lexp->eval(state);
	int second_number = line.find("THEN");
	string rightExpr = line.substr(number + 1, second_number - 1 - number);
	scanner.setInput(rightExpr);
	scanner.ignoreWhitespace();
	Expression *rexp = parseExp(scanner);
	int rvalue = rexp->eval(state);
	stringstream ss(line.substr(second_number + 4));
	int nextLine;
	ss >> nextLine;
	if (type == 0) {
		if (lvalue == rvalue) {
			this->nextLineNumber = nextLine;
		}
		else this->nextLineNumber = __NO_RETURN;
	}
	else if (type == 2) {
		if (lvalue > rvalue) {
			this->nextLineNumber = nextLine;
		}
		else this->nextLineNumber = __NO_RETURN;
	}
	else if (type == 1) {
		if (lvalue < rvalue) {
			this->nextLineNumber = nextLine;
		}
		else this->nextLineNumber = __NO_RETURN;
	}
}

int IfStatement::returnLineNumber(int num)
{
	return nextLineNumber;
}

RemStatement::RemStatement(string line)
{
	this->line = line;
}

void RemStatement::execute(EvalState & state)
{
}

int RemStatement::returnLineNumber(int num)
{
	return __NO_RETURN;
}

EndStatement::EndStatement()
{

}

EndStatement::EndStatement(string line)
{
	this->line = line;
}

void EndStatement::execute(EvalState & state)
{
}

int EndStatement::returnLineNumber(int num)
{
	return __STOP_SIG;
}

GotoStatement::GotoStatement(string line)
{
	this->line = line;
}

void GotoStatement::execute(EvalState & state)
{
}

int GotoStatement::returnLineNumber(int num)
{
	TokenScanner scanner;
	scanner.ignoreWhitespace();
	scanner.setInput(line);
	string t = scanner.nextToken();
	if (!scanner.hasMoreTokens()) error("SYNTAX ERROR\n");
	else
	{
		int lineNumber = stringToInteger(scanner.nextToken());
		this->terminal = lineNumber;
		return lineNumber;
	}
}

PrintStatement::PrintStatement(string line)
{
	this->line = line;
}

void PrintStatement::execute(EvalState & state)
{
	TokenScanner scanner;
	scanner.setInput(line);
	scanner.ignoreWhitespace();
	string tmp = scanner.nextToken();
	Expression *exp = parseExp(scanner);
	int value = exp->eval(state);
	cout << value << endl;
	delete exp;
}

int PrintStatement::returnLineNumber(int num)
{
	return __NO_RETURN;
}

InputStatement::InputStatement(string line)
{
	this->line = line;
}

void InputStatement::execute(EvalState & state)
{
	stringstream ss(line);
	string word;
	ss >> word;
	string var;
	ss >> var;
	string input;
	TokenScanner scanner;
	scanner.setInput(line);
	scanner.ignoreWhitespace();
	string tmp = scanner.nextToken();
	bool signflag = false;
	while (true) {
		cout << " ? ";
		getline(cin, input);
		int i = 0;
		while (i < input.size()) {
			if (input[i] == '-' && !signflag) {
				signflag = true;
				i++;
				continue;
			}
			if (input[i]<'0' || input[i]>'9') {
				cout << "INVALID NUMBER\n";
				i = input.size() + 1;
			}
			i++;
		}
		if (i == input.size())break;
	}
	scanner.setInput(input);
	scanner.ignoreWhitespace();
	if (signflag) string tmp = scanner.nextToken();
	Expression *exp = parseExp(scanner);
	int value = exp->eval(state);
	if (signflag) value = -value;
	state.setValue(var, value);
	delete exp;
}

int InputStatement::returnLineNumber(int num)
{
	return __NO_RETURN;
}
