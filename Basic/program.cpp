/*
 * File: program.cpp
 * -----------------
 * This file is a stub implementation of the program.h interface
 * in which none of the methods do anything beyond returning a
 * value of the correct type.  Your job is to fill in the bodies
 * of each of these methods with an implementation that satisfies
 * the performance guarantees specified in the assignment.
 */

#include <string>
#include "program.h"
#include "statement.h"
using namespace std;

Program::Program() {
   // Replace this stub with your own code
}

Program::~Program() {
   // Replace this stub with your own code
}

void Program::clear() {
	map<int, string> transfer;
	ProgramCode = transfer;
	map<int, Statement*> tmp;
	this->StatementSet = tmp;
   // Replace this stub with your own code
}

void Program::addSourceLine(int lineNumber, string line) {
	map<int, string>::iterator ite = ProgramCode.find(lineNumber);
	if (ite != ProgramCode.end()) ProgramCode[lineNumber] = line;
	else 
   // Replace this stub with your own code
	ProgramCode.insert(map<int, string>::value_type(lineNumber, line));
}

void Program::removeSourceLine(int lineNumber) {
	map<int, string>::iterator ite = ProgramCode.find(lineNumber);
	if (ite != ProgramCode.end())ProgramCode.erase(lineNumber);
}

string Program::getSourceLine(int lineNumber) {
	map<int, string>::iterator ite = ProgramCode.find(lineNumber);
	if (ite != ProgramCode.end())
		return ite->second;
	else return __LINE_NOT_FOUND_ERROR;
   //return "";    // Replace this stub with your own code
}

void Program::setParsedStatement(int lineNumber, Statement *stmt) {
	map<int, Statement*>::iterator ite = StatementSet.find(lineNumber);
	if (ite != StatementSet.end()) StatementSet[lineNumber] = stmt;
	else
	StatementSet.insert(map<int, Statement*>::value_type(lineNumber, stmt));
   // Replace this stub with your own code
}

Statement *Program::getParsedStatement(int lineNumber) {
	map<int, Statement*>::iterator ite = StatementSet.find(lineNumber);
	if (ite != StatementSet.end()) return ite->second;
	else return __LINE_NOT_FOUND_STATEMENT_ERROR;
   //return NULL;  // Replace this stub with your own code
}

int Program::getFirstLineNumber() {
	map<int, string>::iterator ite = ProgramCode.begin();
   return ite->first;     // Replace this stub with your own code
}

int Program::getNextLineNumber(int lineNumber) {
	map<int, string>::iterator ite = ProgramCode.find(lineNumber);
	ite++;
	if (ite != ProgramCode.end())
		return ite->first;
	else return __LINE_NO_MORE;
    //return 0;     // Replace this stub with your own code
}

bool Program::queryLineNumber(int lineNumber)
{
	map<int, string>::iterator ite = ProgramCode.find(lineNumber);
	if (ite != ProgramCode.end()) return true;
	else return false;
}

